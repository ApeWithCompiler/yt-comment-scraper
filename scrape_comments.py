#!/usr/bin/python3

import sys
from datetime import datetime
import sqlite3
import db
import yt


class Comment(object):
    """docstring for Comment"""
    def __init__(self, Id, ParentId, VideoId, TextDisplay, TextOriginal, AuthorDisplayName, AuthorChannelId, publishedAt, updatedAt):
        super(Comment, self).__init__()
        self.id = Id
        self.parentId = ParentId
        self.videoId = VideoId
        self.textDisplay = TextDisplay
        self.textOriginal = TextOriginal
        self.authorDisplayName = AuthorDisplayName
        self.authorChannelId = AuthorChannelId
        self.publishedAt = publishedAt
        self.updatedAt = updatedAt
        


def get_next_scrape_id(con, VideoId):
    last_id = db.max_scrape_id(con, VideoId)
    if last_id is None:
        return 1
    else:
        return last_id + 1


def get_next_page_token(CommentThreadResponse):
    if "nextPageToken" in CommentThreadResponse:
        return CommentThreadResponse["nextPageToken"]
    else:
        return None


def serialize_comment_threads(CommentThreadResponse):
    comments = []

    for commentthread in CommentThreadResponse["items"]:
        commentthread_data = commentthread["snippet"]["topLevelComment"]
        comments.append(Comment(
            commentthread_data["id"],
            "",
            commentthread_data["snippet"]["videoId"],
            commentthread_data["snippet"]["textDisplay"],
            commentthread_data["snippet"]["textOriginal"],
            commentthread_data["snippet"]["authorDisplayName"],
            commentthread_data["snippet"]["authorChannelId"]["value"],
            commentthread_data["snippet"]["publishedAt"],
            commentthread_data["snippet"]["updatedAt"]
            ))

        if "replies" in commentthread:
            for reply in  commentthread["replies"]["comments"]:
                comments.append(Comment(
                    reply["id"],
                    reply["snippet"]["parentId"],
                    reply["snippet"]["videoId"],
                    reply["snippet"]["textDisplay"],
                    reply["snippet"]["textOriginal"],
                    reply["snippet"]["authorDisplayName"],
                    reply["snippet"]["authorChannelId"]["value"],
                    reply["snippet"]["publishedAt"],
                    reply["snippet"]["updatedAt"]
                    ))

    return comments  


def main():
    if len(sys.argv) < 3:
        print("Error: parameters missing")
        print("Usage: VideoID DatabaseFile")
        return

    video_id = sys.argv[1]
    db_file = sys.argv[2]

    yt_api_requet_cnt = 0

    con = sqlite3.connect(db_file)
    scrape_id = get_next_scrape_id(con, video_id)

    #"W1YPPoo-zgA"
    reply = yt.get_comment_threads(video_id)
    yt_api_requet_cnt += 1
    scrape_ts = datetime.now()
    
    comments = serialize_comment_threads(reply)
    next_page_token = get_next_page_token(reply)

    for comment in comments:
        db.insert_comment(con, comment, scrape_id, scrape_ts)

    con.close()
    
    print("Used api requests: " + yt_api_requet_cnt)

if __name__ == '__main__':
    main()
