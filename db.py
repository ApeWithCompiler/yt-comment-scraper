#!/usr/bin/python3

import sqlite3

def init(con):
    cur = con.cursor()
    cur.execute("CREATE TABLE comments (id text, parent_id text, video_id text, text_orig text, text_display text,author_displayname text, author_channelid text, published_at text, updated_at text, scrape_id integer, scraped_at timestamp)")
    con.commit()

    cur.execute("CREATE INDEX index_comment_id ON comments(id)")
    con.commit()

    cur.execute("CREATE INDEX index_comment_videoid ON comments(video_id)")
    con.commit()

    cur.execute("CREATE INDEX index_comment_scrapeid ON comments(scrape_id)")
    con.commit()

    cur.execute("CREATE INDEX index_comment_scrapedts ON comments(scraped_at)")
    con.commit()


def comment_allready_exists(con, CommentId):
    cur = con.cursor()
    cur.execute("SELECT count(*) FROM comments WHERE id = ?", (CommentId,))
    data = cur.fetchone()[0]
    cur.close()
    if data == 0:
        return False
    else:
        return True

# Returns None if empty or Id
def max_scrape_id(con, VideoId):
    cur = con.cursor()
    cur.execute("SELECT MAX(scrape_id) from comments WHERE video_id = ?", (VideoId,))
    data = cur.fetchone()[0]
    cur.close()
    return data


def insert_comment(con, Comment, ScrapeId, ScrapedTs):
    cur = con.cursor()

    insert_data = (
        Comment.id,
        Comment.parentId,
        Comment.videoId,
        Comment.textOriginal,
        Comment.textDisplay,
        Comment.authorDisplayName,
        Comment.authorChannelId,
        Comment.publishedAt,
        Comment.updatedAt,
        ScrapeId,
        ScrapedTs
    )

    cur.execute("""INSERT INTO comments 
                (id,parent_id,video_id,text_orig,text_display,author_displayname,author_channelid,published_at,updated_at,scrape_id,scraped_at)
                VALUES (?,?,?,?,?,?,?,?,?,?,?)""",
                insert_data)
    
    con.commit()
    cur.close()