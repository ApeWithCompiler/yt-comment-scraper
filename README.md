# Youtube Comment Scraper

## Usage

This tool is meant to scrape comments from a youtube video and store them in a sqlite database.

**This is poc and lacs every sort of convinience or quality for that matter!**

### Initialize new database

It is required to explicitly initialize a new sqlite dabase.


```
./create_db.py <database>
```

### Set API Key

To set the api key edit the `API_KEY` variable in the `yt.py` file.

### Scrape comments

To scrape comments run the script with the youtube video id and database file as parameter.
```
./scrape_comments.py <videoID> <database>
```
