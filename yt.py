#!/usr/bin/python3

import requests

API_KEY=""

def get_comment_threads(VideoID, ResourceParts="snippet,replies", MaxCount=100, PageToken=None):
    API_ENDPOINT_URL="https://www.googleapis.com/youtube/v3/commentThreads?key={}&videoId={}&part={}&maxResults={}"

    url = API_ENDPOINT_URL.format(API_KEY, VideoID, ResourceParts, MaxCount)
    if PageToken is not None:
        url = url + "&pageToken={}".format(PageToken)
    resp = requests.get(url)
    if resp.status_code != 200:
        raise
    return resp.json()
