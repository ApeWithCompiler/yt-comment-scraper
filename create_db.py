#!/usr/bin/python3

import sys
import db
import sqlite3

def main():
    if len(sys.argv) == 1:
        print("Error: database name needed!")
        return

    con = sqlite3.connect(sys.argv[1])
    db.init(con)
    con.close

if __name__ == '__main__':
    main()