import sys
import db
import sqlite3

def main():
    if len(sys.argv) < 3:
        print("Error: parameters missing")
        print("Usage: VideoID DatabaseFile")
        return

    video_id = sys.argv[1]
    db_file = sys.argv[2]


    con = sqlite3.connect(db_file)
    data = db.max_scrape_id(con, video_id)
    con.close

    print(data)

if __name__ == '__main__':
    main()